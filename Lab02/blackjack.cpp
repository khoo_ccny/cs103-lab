// A simple blackjack scoring program

#include <iostream>
using namespace std;

int main()
{
	// Declaring my variables
	int m_numOfHands = 0, m_total = 0, m_total2 = 0; // Have two running scores to keep track of two possible aces.
	char cardVal;
	bool isAce = false;
	
	// Prompt user for number of cards
	cout << "How many cards do you have? ";
	cin >> m_numOfHands;
	
	// Check for valid input
	if (m_numOfHands < 2 || m_numOfHands > 5)
	{
		cout << "Invalid input, please try again.\n";
		return 0;
	}
	
	// For each hand, ask for its card value and increment the scores accordingly
	for (int i = 0; i < m_numOfHands; i++)
	{
		cout << "Please enter the card values for card " << i << ": ";
		cin >> cardVal;
		
		// Capture various card value
		if (cardVal == '2')
		{
			m_total		+= 2;
			m_total2	+= 2;
		}
		else if (cardVal == '3')
		{
			m_total		+= 3;
			m_total2	+= 3;
		}
		else if (cardVal == '4')
		{
			m_total		+= 4;
			m_total2	+= 4;
		}
		else if (cardVal == '5')
		{
			m_total		+= 5;
			m_total2	+= 5;
		}
		else if (cardVal == '6')
		{
			m_total		+= 6;
			m_total2	+= 6;
		}
		else if (cardVal == '7')
		{
			m_total		+= 7;
			m_total2	+= 7;
		}
		else if (cardVal == '8')
		{
			m_total		+= 8;
			m_total2	+= 8;
		}
		else if (cardVal == '9')
		{
			m_total		+= 9;
			m_total2	+= 9;
		}
		else if ((cardVal == 't') || (cardVal == 'T') || (cardVal == 'j') || (cardVal == 'J') || (cardVal == 'q') || (cardVal == 'Q') || (cardVal == 'k') || (cardVal == 'K'))
		{
			m_total		+= 10;
			m_total2	+= 10;
		}
		else if ((cardVal == 'a') || (cardVal == 'A'))
		{
			// Ace is encountered
			isAce		= true;
			
			// This is a greedy (maximizing) approach, such that always keep the highest score
			// Of course, this is the easiest approach and there are others, possibly complex approaches
			m_total2	= m_total;
			
			// Increment scores using two different aces
			m_total		+= 11;	// Ace as 11
			m_total2	+= 1;	// Ace as 1
		}
		else
		{
			// Perhaps the user type the wrong value
			cout << "Invalid input, please try again!\n";
			i--; 	// Have user redo, "reversing" the loop iteration by decrementing i (the loop counter)
			
			// NOTES:
			// The loop counter is important and you must be careful in changing its value. A mistake can leads to infinite loop or undesirable behavior.
		}
	}
	
	// Pick the highest of the two scores that didn't go over 21
	if (isAce && m_total > 21 && m_total2 < m_total)
		m_total = m_total2;
	
	// Output result: BUSTED or card value
	if (m_total > 21)
		cout << "BUSTED\n";
	else
		cout << "Your score is " << m_total << endl;
	
	return 0;
}