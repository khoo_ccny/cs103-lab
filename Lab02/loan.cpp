// Loan payment calculation program

#include <iostream>
#include <iomanip> 		// Allows you to manipulate I/O. For more info: http://en.cppreference.com/w/cpp/io/manip
using namespace std;

int main()
{
	// Declaring my variables and setting default values to 0
	double 	m_loanAmt 		= 0.0;
	double 	m_interestRate	= 0.0;
	double 	m_mthPayment	= 0.0;
	double 	m_interestPaid	= 0.0;
	double 	interest		= 0.0;
	int		m_month			= 0;
	
	// Prompt user for loan amount and interest rate
	cout << "Please enter the loan amount and interest rate: ";
	cin >> m_loanAmt >> m_interestRate;
	
	// Calculate monthly payment based on assumption in the problem
	m_mthPayment = m_loanAmt / 20.0; 
	cout << "\nMonthly payment is $" << m_mthPayment << endl;
	
	// While there's still some balance to pay, calculate interest and remaining balance
	while (m_loanAmt > 0.0)
	{
		// Calculate interest for current month
		interest = (m_interestRate * m_loanAmt) / 12.0;
		m_interestPaid += interest; 			// Keep track of total interest paid so far
		
		m_loanAmt -= (m_mthPayment - interest);	// Re-compute balance after paying off interest
		m_month++;								// Increment (pseudo) time. Though it has no effect in payment calculation
	}
	
	// Output results
	cout << "After " << m_month << " months, your loan will be paid off and the interest you have paid over the life of the loan is $" << fixed << setprecision(2) << m_interestPaid << endl;
	
	return 0;
}