/**
	Problem Statement:
				Read the header file (dayofyear.h) for documentation. Implement those functions in a separate implementation file. Write a main function (this file) to test out your user defined class (data type).
				
	Compile:	g++ -c dayofyear.cpp main.cpp
				g++ dayofyear.o main.o -o dayofyear
	Execute: 
				./dayofyear
**/

#include "dayofyear.h"

int main()
{
	DayOfYear today, someone_birthday; // Declare 2 variables of DayOfYear
	
	// Prompt the user to input today's date and display it
	cout << "Enter today's date:\n";
	today.input();
	cout << "Today's date is ";
	today.output(); // Output the month and day of today
	
	// Manually set birthday to be (1,1) (January 1st) and display it.
	someone_birthday.set(1, 1);
	cout << "Someone's birthday is ";
	someone_birthday.output(); // Output the month and day of someone_birthday
	
	/*
		Compare the month and day of each variable to see if today is someone's birthday.
		today.get_month() returns the month from today
		someone_birthday.get_month() returns the month from someone_birthday
		Similarly, for get_day().
	*/
	if(today.get_month() == someone_birthday.get_month() && today.get_day() == someone_birthday.get_day())
		cout << "Happy Birthday!\n";
	else
		cout << "Happy Unbirthday, whoever you are!\n";
		
	return 0;
}