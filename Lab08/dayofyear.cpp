#include "dayofyear.h"

/**
	Function:		DayOfYear()
	Note:
					Initialize all variables to default
**/
DayOfYear::DayOfYear()
{
	month = 0;
	day = 0;
}

/**
	Function:		void input()
	Note:
					Prompt the user for input and call check_date() to validate date
**/
void DayOfYear::input()
{
	cout << "Enter the month as a number: ";
	cin >> month;
	cout << "Enter the day of the month: ";
	cin >> day;
	check_date();
}

/**
	Function:		void output()
	Note:
					Output a simple message with month and day
**/
void DayOfYear::output()
{
	cout << "month = " << month << ", day = " << day << endl;
}

/**
	Function:		void set(int new_month, int new_day)
	Parameters:
					new_month	A new month value, may or may not be valid
					new_day		A new day value, may or may not be valid
	Note:
					Update month and day and call check_date() to validate date
**/
void DayOfYear::set(int new_month, int new_day)
{
	month = new_month;
	day = new_day;
	check_date();
}

/**
	Function:		int get_month()
	Note:
					Return month
**/
int DayOfYear::get_month()
{
	return month;
}

/**
	Function:		int get_day()
	Note:
					Return day
**/
int DayOfYear::get_day()
{
	return day;
}

/**
	Function:		void check_date()
	Note:
					Check to make sure month and day are within valid range. Assuming each month has 31 days. If not, quit the program by calling exit(1);
**/
void DayOfYear::check_date()
{
	if((month < 1) || (month > 12) || (day < 1) || (day > 31))
	{
		cout << "Illegal date. Aborting program\n";
		exit(1);
	}
}