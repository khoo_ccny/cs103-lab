/* 
	dayofyear.h
	This is the header file for a simple class for entering and displaying date
	We are only concern with month and day for this program.
*/

#pragma once

#include<iostream>
#include<cstdlib>
using namespace std;

class DayOfYear
{
public:
	/*
		Constructor
	 	Initialize all variables to default values (i.e. 0)
	*/
	DayOfYear(); 
	
	/*
		Prompt the user to input a date.
		Validate the date
	*/
	void input(); 
	
	/*
		Print the month and day
	*/
	void output();
	
	/*
		Reset the date according to the new arguments; update the month and day
		Validate the new date
	*/
	void set(int new_month, int new_day);
	
	/*
		returns the month
	*/
	int get_month();
	
	/*
		returns the day
	*/
	int get_day();
	
private:
	/*
		Private member function
		Make sure month and day are in valid range.
		You can assume each month has 31 days.
		You can use exit(1) to end the program
	*/
	void check_date();
	
	int month;	// variable to hold the month
	int day;	// variable to hold the day
};

// TODO
/*
	dayofyear.cpp
	Implement the above functions in a separate implementation file named dayofyear.cpp
*/
/*
	main.cpp
	Write your own main function that utilize this class.
	Suggestion:
		Have the user enter today's date and set a person's birthday to a pre-defined date. Check to see if today is that person's birthday, output an appropriate message accordingly.
*/
