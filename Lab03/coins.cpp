#include <iostream>
using namespace std;

// Function Prototype
void compute_coins(int, int&, int&, int&);

int main () {
    int coins, quarters, dimes, pennies;
    cout << "Enter the number of cents: ";
    cin >> coins;
    
    // Function Call
    // We receive 3 outputs through references
    compute_coins(coins, quarters, dimes, pennies);
    
    cout << "Your change will be "
        << quarters << " quarter(s) "
        << dimes << " dime(s) "
        << pennies << " pennies" << endl;
    
    // The following shows that the function compute_coins(...)
    // did not change the value of coins.
    cout << "The value of coins is " << coins << endl;
    
    // TODO: Make the program ask the user if they want to continue
    //  making change.
    // TODO: Try solving this problem with quarters, nickels and pennies
    //  instead.
    // TODO: Try solving a similar problem where the user enters the
    //  number of seconds elapsed, and your program outputs how many
    //  hours, minutes and seconds that is.
    
    return 0;
}

// Function Implementation
void compute_coins(int coin_value, int& q, int& d, int& p) 
{
    // In the following we must start with the largest denomination
    //  coin, and continue until you've gone through all of the coin
    //  denominations.
    int coins_left;
    
    q = coin_value / 25;
    coins_left = coin_value % 25;
    
    d = coins_left / 10;
    coins_left = coins_left % 10;
    
    p = coins_left;
    
    // Since coin_value was passed-by-value it is a
    // variable only available in this function.
    // So the following will NOT have an effect on the
    // variable coins in the main function.
    coin_value = 12345;
}

