/**
	Problem Statement:
				Write a recursive program to perform one of the following functions on the first k integers: add, multiply, sum of squared
				
	Compile:	g++ -Wall recur.cpp -o recur
	Execute: 
				./recur [choice] [integer]
**/
#include<iostream>
#include<cstdlib>
using namespace std;

// Function prototype
int recursion(int integer, int choice);

int main(int argc, char *argv[])
{
    // Program settings
    if((argc < 3) || (argc > 3))
    {
        cout << "Invalid input!\n";
        cout << "./recur [choice] [integer]\n";
        exit(1);
    }
    
    // Get program parameter values
    int choice = atoi(argv[1]);
	int input = atoi(argv[2]);

    // Error checking
	if(input < 0)// can't perform negative integer.
	{
		cout << "Integer can't be negative\n";
		exit(1);
	}

    // Call the recursion function with an appropriate output message
	if(1 == choice)
		cout << "The sum of the first " << input << " positive integers is: " << recursion(input, choice) << endl;
	else if(2 == choice)
		cout << "The product of the first " << input << " positive integers is: " << recursion(input, choice) << endl;
	else
		cout << "The sum of the first " << input << " squared positive integers is: " << recursion(input, choice) << endl;
		
	return 0;
}

/**
	Function:		int recursion(int integer, int choice)
	Parameter:	
					integer		An integer value
                    choice      The operation choice
	Note:
					Add             => T(n) = T(n - 1) + n
                    Mult            => T(n) = T(n - 1) * n
                    Sum of Squared  => T(n) = T(n - 1) + (n^2)
**/
int recursion(int integer, int choice)
{
	if(1 == integer)
		return 1;   // base case
	else
	{   //recursion depending on user's choice.
		if(1 == choice)
			return integer + recursion(integer - 1, choice);
		else if(2 == choice)
			return integer * recursion(integer - 1, choice);
		else
			return integer * integer + recursion(integer - 1, choice);
	}
}
