/**
	Compile:	g++ -Wall caesarCipher.cpp -o caesarCipher
	Execute: 
				./caesarCipher
	Note:		-Wall tells the compiler to display all warnings, if any. -W is the warning option. all specify all
				Reference to caesar cipher: http://en.wikipedia.org/wiki/Caesar_cipher
				Reference to ASCII table: http://www.asciitable.com/
**/

#include<iostream>
#include<string>
#include<cctype> // provide tolower()
using namespace std;

// Macro variables; search and replace the name before compiling
// Global, not modifiable
// An easy way to configure parameters for your program; you don't have to hunt down all the places that you want to change the values, you can change it here
// For example, if I want my program to work over a different range, I can change the low and high values here and the rest of the code will work as before.
#define LOW_RANGE 32
#define HIGH_RANGE 126
#define RANGE (HIGH_RANGE - LOW_RANGE)

/*
Like regular variables, but global. Memory will be allocated to hold these variables. Can modify.
Could be constant or not
const int low_range = 32;
const int high_range = 126;
const int range = high_range - low_range;
*/

// Function prototype for the cipher
void caesarCipher(string& text, int shift);

int main()
{
	// Variables declarations
	int m_shift, m_choice;
	char resp;
	string m_text;
	
	do {
		cout << endl;
		cout << "1) Encryption\n"; // positive shift
		cout << "2) Decryption\n"; // negative shift
		cin	>> m_choice;
		
		// Checking validity of choice
		if (m_choice != 1 && m_choice != 2)
		{
			cout << "Invalid Input!\n";
			break;
		}
		
		cout << "Input a positive shifting distance: ";
		cin >> m_shift;
		
		// I only want positive shifting distance
		if(m_shift > 0)
		{
			// Takes care of negative shift, if it's decryption; negate the shift value
			if (m_choice == 2)
				m_shift *= -1;
				
			// Output proper prompt based on input choice
			if(m_choice == 1)
				cout << "Input a plaintext:\n";
			else
				cout << "Input a ciphertext:\n";

			cin.ignore(); // clear input buffer
			getline(cin, m_text); // get string from user
			
			caesarCipher(m_text, m_shift); // perform caesar cipher

			// Output proper prompt based on input choice
			if (m_choice == 1)
				cout << "Encrypted text is:\n";
			else
				cout << "Decrypted text is:\n";
			
			// Output the encrypted or decrypted text
			cout << m_text << endl;
		}
		else
		{
			cout << "Distance has to be positive.\n";
		}
		
		// Continue?
		cout << "Continue? [y/n]: ";
		cin >> resp;
	} while(tolower(resp) == 'y');
	
	return 0;
}

/**
	Function:		void caesarCipher(string& text, int shift)
	Parameter:	
					text	Input string, by reference
					shift	shifting distance; positive for encryption, negative for decryption
	Note:
					Because we're interested in a specific range of values, we need to do offsetting in order to do wrap around
**/
void caesarCipher(string& text, int shift)
{
	int word;	// to store ASCII value
	int index;	// to iterate through the string
	
	index = 0;
	
	// while not at the end of the string, perform shift
	while(text[index] != '\0') // '\0' is the NULL terminated character
	{
		word = text[index] + shift; // apply shift
		
		// Boundary checking, perform wrap around if needed
		if (word > HIGH_RANGE) // Due to encryption
		{
			// Offset word, perform modulus to find the appropriate location word gets wrapped to, then undo offset
			word = LOW_RANGE + (word - HIGH_RANGE + 1) % RANGE;
		}
		else if(word < LOW_RANGE) // Due to decryption
		{
			// Similar to above
			word = HIGH_RANGE - (LOW_RANGE - 1 - word) % RANGE;
		}
		
		// Assign the shifted ASCII back to char and increment index
		text[index] = word;
		index++;
	}
}