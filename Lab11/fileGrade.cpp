/**
	Problem Statement:
				Read in a file. Compute the average of 10 grades for each student. And writes the result to an output file. The input file format is as follows:
                
                    number_of_grades_for_each_student
                    last_name_1 first_name_1 grade_1 grade_2 ... grade_n
                    last_name_2 first_name_2 grade_1 grade_2 ... grade_n
                    .
                    .
                    .
                    
                The output file format is the same as above except an additional last column which shows the average grades for that student.
				
	Compile:	g++ -Wall -std=c++11 fileGrade.cpp -o fileGrade
	Execute: 
				./fileGrade
    Note:
                Reference to file stream (fstream): http://www.cplusplus.com/reference/fstream/fstream/
                It also contain reference to input stream (ifstream) and output stream (ofstream)
**/
#include <iostream>
#include <fstream>
#include <cstdlib> // needed for exit()
#include <string>
using namespace std;

int main(int argc, char *argv[])
{
    // Declare variables
	string filenameIn, filenameOut; // files
	string lastname, firstname;     // names
	int grade, numGrade;            // grades
	double avg;                     // average
	ifstream inFile;                // input file
	ofstream outFile;               // output file
	
    // Prompt for input and output filename
	cout << "Please type in the INPUT filename or path: ";
	getline(cin, filenameIn);
	
	cout << "\nPlease type in the OUTPUT filename or path: ";
	getline(cin, filenameOut);
	
    // Open input file and check to see if it's open successfully
	inFile.open(filenameIn);
	if (inFile.fail()) // check for successful open
	{
		cout << "\nThe input file was not successfully opened"
			 << "\n Please check that the file currently exists.\n";
		exit(1);
	}
	
    // Open output file and check to see if it's open successfully
	outFile.open(filenameOut);
	if (outFile.fail())
	{
		cout << "\nThe output file was not successfully opened\n";
		exit(1);
	}
    
    // Read in the number of grades for each student
    inFile >> numGrade; // read in number of grades
		
	// read input file
    // check whether the stream is good (while there's still something to read) or not
	while (inFile.good()) 
    //while(!inFile.eof()) // alternate way of looping by checking is it end-of-file yet
	{
		avg = 0.0;          // Reset average
        
        // Read in last and first name for each student and write it directly to the output file.
		inFile >> lastname >> firstname;
		outFile << lastname << " " << firstname << " "; // Output with spaces for better formatting
		
        // Read in each grade for each student
		for (int i = 0; i < numGrade; i++)
		{
			inFile >> grade;
			outFile << grade << " ";
            
            // Accumulate as each grade is read
			avg += grade;
		}
        
		avg /= numGrade;        // Compute the average
		
		outFile << avg << endl; // Write the average to output file
        
        // At this point (end of each iteration), it has processed the entire line. Next iteration will process the next line in the file
	}
	
    // Alert user that processing is done.
	cout << "\nDone.  The output has been written to " << filenameOut << endl;
	
    // Close all the files and do any other clean up as needed
	inFile.close();
	outFile.close();
    
	return 0;
}