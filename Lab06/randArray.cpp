/**
	Problem Statement:
				Create an array with randomly generated values. Sort the array. And find the max and min values, and its corresponding position in the original array.
				
	Compile:	g++ -Wall randArray.cpp -o randArray
	Execute: 
				./randArray [size] [low_range] [high_range]
	Note:		-Wall tells the compiler to display all warnings, if any. -W is the warning option. all specify all
				Reference to bubble sort: http://en.wikipedia.org/wiki/Bubble_sort
				Reference to command line arguments: http://www.cplusplus.com/articles/DEN36Up4/
				Reference to switch statement: http://www.cplusplus.com/doc/tutorial/control/
				Reference to atoi: http://www.cplusplus.com/reference/cstdlib/atoi/
**/

#include<iostream>
#include<ctime>		// provide time()
#include<cstdlib>	// provide srand() and atoi()
using namespace std;

// Function prototypes
void printArray(int*, int);			// print array
void sortArray(int*, int);			// sort array using bubble sort in descending order
int findPosition(int*, int, int);	// given a value, find its position in the original array
string suffix(int);					// figure out the proper suffix for a number

int main(int argc, char *argv[])	// command line arguments; the program name will always get passed into main() as argv[0]
{
	// Default values that can be changed via command line
	int m_size = 10;				// size of the array
	int low_range = 0;				// lower bound for rand()
	int high_range = 100;			// upper bound for rand()
/*	
	// This is equivalent to the switch construct below
	if (2 == argc)
		m_size = atoi(argv[1]);
	else if ((3 == argc) || (argc > 4))
	{
		cout << "Insufficient input! Correct usage:\n";
		cout << "./randArray [size] [low_range] [high_range]\n";
		return 0;
	}
	else if (4 == argc)
	{
		m_size = atoi(argv[1]);
		low_range = atoi(argv[2]);
		high_range = atoi(argv[3]);
	}
*/
	
	// switch on the int argc
	switch(argc)		// argc is the expression to evaluate
	{
		case 4: // If argc is 4, execute this block
			low_range = atoi(argv[2]);	// Convert the 3rd argument from char to int; specify lower bound
			high_range = atoi(argv[3]);	// convert the 4th argument; specify upper bound
			// Instead of breaking, I want to execute the same instruction for case 2; this is called stacking
		case 2: // If argc is 2, execute this block
			m_size = atoi(argv[1]);		// Convert the 2nd argument; specify size of array
			break;
		case 1: // If argc is 1, execute this block
			// Only program name is passed, no extra parameters are passed. Use default values as specified above
			break;
		default: // Otherwise (else)
			// User specified the wrong number of arguments
			// Perhaps we should be nice and tell them the proper number of arguments and what they are.
			// You can be even nicer by explaining (via cout) what each parameter do and give an example 
			cout << "Insufficient input! Correct usage:\n";
			cout << "./randArray [size] [low_range] [high_range]\n";
			return 0; 					// Since they give you the wrong number of arguments, don't even run the program and simply return from main.
	}
	
	// Declare array and other variables
	// We should declare the array here because m_size might be different depending on what the user specified in the command line
	int m_num[m_size], m_origNum[m_size], m_maxPos, m_minPos;
	
	// Seed the random number generator
	// Without seeding, the random number sequence will be the same each time you execute the code
	// Ideally, you should seed the generator with a pseudo-random number, in this case, we use the current time.
	srand(time(NULL)); 					// time(NULL) gives you the current time
	
	// Use rand to populate the array
	for (int i = 0; i < m_size; i++)
	{
		m_num[i] = low_range + rand() % (high_range - low_range);	// will only generate values between low_range and high_range
		m_origNum[i] = m_num[i];
	}
	
	cout << "Original array:\n";
	printArray(m_num, m_size);			// passing m_num is the same as passing the address of m_num since m_num is an array
	
	sortArray(m_num, m_size);			// use bubble sort; result will be in descending order
	cout << "\nSorted array:\n";
	printArray(m_num, m_size);
	
	m_maxPos = findPosition(m_origNum, m_size, m_num[0]);	// since it's sorted, the max value is at index 0. Use this value to find its original position in m_origNum
	m_minPos = findPosition(m_origNum, m_size, m_num[m_size - 1]);	// Similar to above; min value is at index m_size - 1
	
	// Output results with proper suffix
	cout << "\nThe maximum number is the " << m_maxPos << suffix(m_maxPos) << " position of the original input: " << m_num[0];
	
	cout << "\nThe minimum number is the " << m_minPos << suffix(m_minPos) << " position of the original input: " << m_num[m_size - 1];
	return 0;
}

/**
	Function:		void printArray(int* p, int size)
	Parameter:	
					p		the array; it reads as "point to the memory address being provided
					size	size of the array
	Note:
					The use of pointer and array is interchangeable.
**/
//void printArray(int p[], int size)	// another way to declare function prototype; however, you will need to use p[i] to access element
void printArray(int* p, int size)
{
	for (int i = 0; i < size; i++)
	//	cout << p[i] << " ";		// another way to access element
		cout << *(p + i) << " ";	// print each element in the array
		
	cout << endl;
}

/**
	Function:		void sortArray(int* p, int size)
	Parameter:	
					p		the array; it reads as "point to the memory address being provided
					size	size of the array
	Note:
					Use bubble sort to sort the array in a descending order
**/
void sortArray(int* p, int size)
{
	// Variables to store current value and number of possible moves
	int temp, moved;
	
	// Start at the 2nd element and loop till the end of the array
	for (int next = 1; next < size; next++)
	{
		temp = p[next];		// store the current value
		moved = next;		// the maximum possible of swap is the same as its index location, since we only swap to the front/left instead of back/right
		
		// while there's moved to make and the current value is greater than the previous (moved - 1) value, swap
		while(moved > 0 && (p[moved - 1] < temp))
		{
			p[moved] = p[moved - 1]; 	// swap
			moved--;					// number of possible swap is decremented
		}
		
		// There's either no more moved or current value is less than or equal to the previous value
		p[moved] = temp;				// assign current value to the index location moved, where the value in this location is less than its previous and greater than the values after it.
	}
}

/**
	Function:		int findPosition(int* p, int size, int val)
	Parameter:	
					p		the array; it reads as "point to the memory address being provided
					size	size of the array
					val		the target value to find
	Return:			
					the position of val in p
	Note:
					since we want 1-based index, we return i + 1
**/
int findPosition(int* p, int size, int val)
{
	// Loop through the array
	for (int i = 0; i < size; i++)
	{
		// Found the target value, return its position
		if(p[i] == val)
			return i + 1;
	}
	
	// We looped through the entire array, but didn't find the target. Return an error code (you can capture this error code in the calling function to better handle error
	return -1;
}

/**
	Function:		string suffix(int a)
	Parameter:	
					a		the integer of which we want to know its proper suffix
	Return:			
					the suffix for a
	Note:
					11, 12, and 13 should have the suffix "th"
**/
string suffix(int a)
{
	// Capture the special case and return its value
	// Without this special case handling, 11, 12, and 13 will be handled by the switch statement below which will return the incorrect suffix
	if(11 == a || 12 == a || 13 == a)
		return "th";

	// a % 10 will give you the digit of a, which dictate its suffix
	switch(a % 10)
	{
		case 1: // digit 1; st
			return "st";
			break;
		case 2: // digit 2; nd
			return "nd";
			break;
		case 3: // digit 3; rd
			return "rd";
			break;
		default: // all other digits; th
			return "th";
			// We don't need a break here because after executing the instruction(s), it will exit the switch statement anyway.
	}
	
/*	
	// The equivalent of the switch construct above
	if (a % 10 == 1)
		return "st";
	else if (a % 10 == 2)
		return "nd";
	else if (a % 10 == 3)
		return "rd";
	else
		return "th";
*/
}
