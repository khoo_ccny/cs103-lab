/**
	Problem Statement:
				Write a program using dynamic array to do 2D matrix multiplication. Assume square matrices. Inputs can be obtained from user or randomly generated
				
	Compile:	g++ -Wall matMult_dynamic.cpp -o matMult_dynamic
	Execute: 
				./matMult_dynamic [size]
	Note:		-Wall tells the compiler to display all warnings, if any. -W is the warning option. all specify all
				Reference to matrix multiplication: http://en.wikipedia.org/wiki/Matrix_multiplication
**/
#include<iostream>
#include<ctime>		// provide time()
#include<cstdlib>	// provide srand() and atoi()
using namespace std;

// FUnction prototype
void printMatrix(int**, int);	// print a matrix

int main(int argc, char *argv[])		// command line arguments; the program name will always get passed into main() as argv[0]
{
	// Default value that can be changed via command line
	int m_size = 3;
	
	switch(argc)
	{
		case 2:
			m_size = atoi(argv[1]);		// Convert the 2nd argument from char to int; specify size
		case 1:							// No additional argument supplied; do nothing
			break;
		default:						// Wrong usage. Exit
			cout << "Invalid number of argument!\n";
			cout << "./matMult_dynamic [size]\n";
			return 0;
	}
	
	/*
		Initialize three 2D arrays of size (m_size X m_size)
		The number of stars usually indicate how many dimension the array has.
	*/
	int **m_a, **m_b, **m_c;
    
    // m_a, m_b, and m_c each points to an array of int* (of size m_size)
    m_a = new int*[m_size];
    m_b = new int*[m_size];
    m_c = new int*[m_size];
	
	srand(time(NULL));	// Seed the random number generator
	
	/*
		Going through each element in the matrix and randomly assign a value to it, except the resultant matrix.
		
		To iterate through a 1D array, you need a for (or while) loop. For 2D array, you need 2 nested for loops. In general, to iterate through a N-D array (where N is an integer value), you will need N nested for loops. 
	*/
	for(int i = 0; i < m_size; i++)			// Loop through each row
	{
        // For each row, allocate an array of int (of size m_size)
        m_a[i] = new int[m_size];
        m_b[i] = new int[m_size];
        m_c[i] = new int[m_size];
        
		for(int j = 0; j < m_size; j++)	// Loop through each column; m_a[i].size() returns the number of columns (of row_i)
		{
			m_a[i][j] = rand() % 10;				// Randomly generate value between 0 to 9, inclusive and assign to a
			m_b[i][j] = rand() % 10;				// Similarly for b
			m_c[i][j] = 0;							// Initialize the resultant matrix to be all 0
		}
	}
	
	// Print the content of matrices a and b 
	cout << "Matrix a is \n";
	printMatrix(m_a, m_size);
	cout << "\nMatrix b is \n";
	printMatrix(m_b, m_size);
	
	// Matrix multiplication
	// The first two outer loops can be seen as iterating through the resultant matrix
	for(int i = 0; i < m_size; i++)			// Loop through each  row
	{
		for(int j = 0; j < m_size; j++)	// Loop through each column
		{
			for(int k = 0; k < m_size; k++) // Loop through each element in the row/column
				m_c[i][j] += m_a[i][k] * m_b[k][j]; // Multiply the corresponding k-th element and sum it up
		}
	}
	
	// Print the content of matrix c
	cout << "\nResult is\n";
	printMatrix(m_c, m_size);
    
    // Release memory
    delete [] m_a;
    delete [] m_b;
    delete [] m_c;
	
	return 0;
}

/**
	Function:		void printMatrix(int **mat, int size)
	Parameter:	
					mat		The 2D array.
                    size    Size of the array
**/
void printMatrix(int **mat, int size)
{
	for(int i = 0; i < size; i++)			// Loop through each row
	{
		for(int j = 0; j < size; j++)	// Loop through each column
			cout << mat[i][j] << "\t";				// Print the entry at (i,j)
		cout << endl;			// Notice that this is outside the 2nd for loop but inside the 1st for loop.
	}

}
