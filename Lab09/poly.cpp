#include "poly.h"

// Re-defining the constant, since it's static.
static const int CAPACITY = 15;

/**
	Function:		polynomial()
	Note:
					This polynomial has been create with all zero coefficients, except for coefficient c for the specified degree.
**/
polynomial::polynomial(double c, int degree)
{
	for (int i = 0; i < CAPACITY; i++)
		coef[i] = 0.0;

	coef[degree] = c;
	largest_degree = degree;
}

/**
	Function:		void add_to_coef(double c, int degree)
	Parameters:
					c			A new coefficient
					degree		A new degree
	Note:
					Add c to coef[degree]
					If degree is larger than largest_degree, update it.
**/
void polynomial::add_to_coef(double c, int degree)
{
	coef[degree] += c;

	if (degree > largest_degree)
		largest_degree = degree;
}

/**
	Function:		void assign_coef(double c, int degree)
	Parameters:
					c			A new coefficient
					degree		A new degree
	Note:
					Assign c to coef[degree]
					If degree is larger than largest_degree, update it.
**/
void polynomial::assign_coef(double c, int degree)
{
	coef[degree] = c;
	
	if (degree > largest_degree)
		largest_degree = degree;
}

/**
	Function:		void clear()
	Note:
					Assign 0 to entire array
					Set largest_degree to 0
**/
void polynomial::clear()
{
	for (int i = 0; i < CAPACITY; i++)
		coef[i] = 0.0;
	
	largest_degree = 0;
}

/**
	Function:		double coefficient(int degree) const
	Parameter:
					degree		The degree of the coefficient to retrieve
	Return:
					coefficient of X^degree
	Note:
					Because of const, this function doesn't allow you to modify any of the class member varialbes
**/
double polynomial::coefficient(int degree) const
{
	return coef[degree];
}

/**
	Function:		void print()
	Note:
					Print out the polynomial in a somewhat human readable form.
	TODO:
					You can certainly overload operator<< to print out polynomial
**/
void polynomial::print()
{
	// Print 1st term
	if (largest_degree > 1)
		cout << coef[largest_degree] << "x^" << largest_degree;
	else if (1 == largest_degree)
		cout << coef[largest_degree] << "x";
	else
		cout << coef[largest_degree];
	
	// Print the rest
	// Explicitly print + and - for better format
	// 0 coefficient is not printed
	for (int i = largest_degree - 1; i >= 0; i--)
	{
		// Positive coefficient
		if(coef[i] > 0)
		{
			cout << " + ";
			if (i > 1)
				cout << coef[i] << "x^" << i;
			else if (1 == i)
				cout << coef[i] << "x";
			else
				cout << coef[i];
		}
		else if(coef[i] < 0) // negative coefficient
		{
			cout << " - ";
			/*
				Instead of printing the negative value directly, it prints the absolute value.
				Instead of seeing "- -9.5x", you will see "- 9.5x"
			*/
			if (i > 1)
				cout << abs(coef[i]) << "x^" << i;
			else if (1 == i)
				cout << abs(coef[i]) << "x";
			else
				cout << abs(coef[i]);
		}
	}

	cout<<endl;
}

/**
	Function:		int degree() const
	Return:
					The degree of the polynomial; the largest_degree
	Note:
					Because of const, this function doesn't allow you to modify any of the class member varialbes
**/
int polynomial::degree() const
{
	return largest_degree;
}

/**
	Function:		polynomial operator +(const polynomial& p1, const polynomial& p2)
	Parameter:
					p1		The left operand (of +) polynomial
					p2		The right operand (of +) polynomial
	Return:
					Another polynomial which contains the result of p1 + p2
	Note:
					p1 and p2 are passed in by reference, but they're not modifiable (due to const)
**/
polynomial operator +(const polynomial& p1, const polynomial& p2)
{
	polynomial p3;

	// Find the common terms (of the same degree) of p1 and p2 to sum
	int mindeg = (p1.degree() < p2.degree()) ? p1.degree() : p2.degree();

	// Iterate both polynomial (of the common terms) and sum their coefficients and assigning result to p3
	for (int i = 0; i <= mindeg; i++)
		p3.assign_coef(p1.coefficient(i)+p2.coefficient(i), i);

	// Take care of the remaining terms by simply assigning it to p3
	if (p1.degree() > p2.degree())
	{
		for (int i = p2.degree() + 1; i <= p1.degree(); i++)
			p3.assign_coef(p1.coefficient(i), i);
	}
	else
	{
		for (int i = p1.degree() + 1; i <= p2.degree(); i++)
			p3.assign_coef(p2.coefficient(i), i);
	}

	return p3;
}

/**
	Function:		polynomial operator -(const polynomial& p1, const polynomial& p2)
	Parameter:
					p1		The left operand (of -) polynomial
					p2		The right operand (of -) polynomial
	Return:
					Another polynomial which contains the result of p1 - p2
	Note:
					p1 and p2 are passed in by reference, but they're not modifiable (due to const)
**/
polynomial operator -(const polynomial& p1, const polynomial& p2)
{
	polynomial p3;

	// Find the common terms (of the same degree) of p1 and p2 to subtract
	int mindeg = (p1.degree() < p2.degree()) ? p1.degree() : p2.degree();

	// Iterate both polynomial (of the common terms) and subtract their coefficients and assigning result to p3
	for (int i = 0; i <= mindeg; i++)
		p3.assign_coef(p1.coefficient(i)-p2.coefficient(i), i);

	// Take care of the remaining terms by subtracting it from 0
	if (p1.degree() > p2.degree())
	{
		for (int i = p2.degree() + 1; i <= p1.degree(); i++)
			p3.assign_coef(0.0 - p1.coefficient(i), i);
	}
	else
	{
		for (int i = p1.degree() + 1; i <= p2.degree(); i++)
			p3.assign_coef(0.0 - p2.coefficient(i), i);
	}

	return p3;
}

