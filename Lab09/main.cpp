/**
	Problem Statement:
				Read the header file (poly.h) for documentation. Implement those functions in a separate implementation file. Write a main function (this file) to test out your user defined class (data type).
				
	Compile:	g++ -c poly.cpp main.cpp
				g++ poly.o main.o -o poly
	Execute: 
				./poly
**/

#include "poly.h"
#include <iostream>
using namespace std;

int main()
{
    // Declaring polynomial variables
    polynomial p, p2, p3, p4;

    // Assign some coefficients to p
    p.assign_coef(-5.5, 5); // Equivalent to -5.5x^5
    p.assign_coef(3.3, 3);  // Equivalent to 3.3x^3 
    p.assign_coef(-2.2, 2);
    p.assign_coef(1.1, 1);
    p.assign_coef(10.0, 0);

    // Assign some coefficients to p2
    p2.assign_coef(5.0, 6);
    p2.assign_coef(-4.0, 5);
    p2.assign_coef(3.3, 3);
    p2.assign_coef(-2.2, 2);
    p2.assign_coef(1.1, 1);
    p2.assign_coef(20.0, 0);

    // Add p and p2 together, and assign the result to p3
    p3 = p + p2;
    p3.print(); // print p3

    // Subtract p2 from p, and assign the result to p4
    p4 = p - p2;
    p4.print(); // print p4

    return 0;
}
