// FILE: poly.h
// CLASS PROVIDED:
//   class polynomial
//     A polynomial has one variable x, real number coefficients, and
//     non-negative integer exponents. Such a polynomial can be viewed
//     as having the form:
//       A[n]*x^n + A[n-1]*x^(n-1) + ... A[2]*x^2 + A[1]*x + A[0]
//     where the A[n] are the real number coefficients and x^i represents
//     the variable x raised to the i power. The coefficient A[0] is
//     called the "constant" or "zeroth" term of the polynomial.
//   
// NOTES TO STUDENT:
//   1. This version works by storing the coefficients in
//      a fixed array. The coefficient for the x^k term is stored
//      in location [k] of the fixed-size array.
//
// MEMBER CONSTANTS
//   const static int CAPACITY
//     The size of the fixed array to store the coefficients.
//
// CONSTRUCTOR for the polynomial class
//   polynomial(double c = 0.0, int degree = 0)
//     This polynomial has been create with all zero
//     coefficients, except for coefficient c for the specified degree.
//     When used as a default constructor (using default values for
//     both arguments), the result is a polynomial with all zero
//     coefficients.
//	   Also keep track of largest degree of the polynomial
//
//   void add_to_coef(double amount, int degree)
//     Adds the given amount to the coefficient of the specified exponent.
//	   Also keep track of largest degree of the polynomial
//
//   void assign_coef(double c, int degree)
//     Sets the coefficient for the specified exponent.
//	   Also keep track of largest degree of the polynomial
//
//   void clear( )
//     All coefficients of this polynomial are set to zero.
//
//   double coefficient(int degree) const
//     Returns coefficient at specified exponent of this polynomial.
//
//   void print()
//	   Output the polynomial using cout in a nicely readable format, it must not 
//	   contain any zero terms (unless all terms are zero). 
//	   Example:
//			3.4x^3 - 2.7x^2 + 7.9x + 4
//
//   int degree( ) const
//     The function returns the value of the largest degree with a non-zero coefficient.
//     If all coefficients are zero, then the function returns zero.
//
//   polynomial operator +(const polynomial& p1, const polynomial& p2)
//     return-value is a polynomial with each coefficient equal to the sum of the 
//	   coefficients of p1 & p2 for any given exponent.
//
//   polynomial operator -(const polynomial& p1, const polynomial& p2)
//     return-value is a polynomial with each coefficient equal to the difference of 
//	   the coefficients of p1 & p2 for any given exponent.

#pragma once

#include <iostream>
#include <cmath>
#include <cstdlib>
using namespace std;

class polynomial
{
	public:
		// CONSTANT
		static const int CAPACITY = 15;

		// CONSTRUCTOR
		polynomial(double c = 0.0, int degree = 0);

		// MEMBER FUNCTIONS
		void add_to_coef(double amount, int degree); 
		void assign_coef(double c, int degree);
		void clear();
		double coefficient(int degree) const;
		void print();
		int degree() const;

	private:
		// DATA MEMBERS
		double coef[CAPACITY];
		int largest_degree;
};

// NON-MEMBER FUNCTIONS
// Overloading operators
polynomial operator +(const polynomial& p1, const polynomial& p2);
polynomial operator -(const polynomial& p1, const polynomial& p2);

// TODO
/*
	poly.cpp
	Implement the above functions in a separate implementation file named poly.cpp
*/
/*
	main.cpp
	Write your own main function that utilize this class.
	Suggestion:
		Declare two polynomial variables.
		Assign some coefficients to them.
		Add or subtract the two variables.
*/
