/**
	Compile:	g++ -Wall numCount.cpp -o numCount
	Execute: 
				./numCount
			or
				./numCount < someNumbers.txt
	Note:		-Wall tells the compiler to display all warnings, if any. -W is the warning option. all specify all
				Reference to vector class: http://www.cplusplus.com/reference/vector/vector/
**/

#include <iostream>
#include <vector>		// Need vector<>
using namespace std;

// Function prototypes declaration.
int maxInVec(vector<int>);				// Find max value in the vector
int countAndErase(vector<int>&, int);	// Count occurrences of a value and erase it from vector

int main()
{
	vector<int> m_numList;				// Input vector that contains a list of integers
	int m_input, m_num, m_count;		// Misc variables to hold input, max value, and count.
	
	// while there's more input, read it.
	// You can terminate input by entering a non-integer value, such as a char 'b' (without the quotes)
	while(cin >> m_input)
	{
		m_numList.push_back(m_input);	// push back or append the input to the vector
	}
	
	// Start outputting
	// Print out column names with tab space (\t)
	cout << "N\tCount\n";
	
	// While m_numList is NOT empty, process it. Notice the exclamation mark
	// Process each row one at a time.
	while(!m_numList.empty())
	{
		m_num 	= maxInVec(m_numList);				// Find max in m_numList
		m_count	= countAndErase(m_numList, m_num);	// Find the number of occurrences of m_num in m_numList
		cout << m_num << "\t" << m_count << endl;		// Print out the max value and its occurrences
	}
	
	return 0;
}

/**
	Function:		int maxInVec(vector<int> list)
	Parameter:	
					list	A copy of the input vector
	Return value:
					The maximum value in the vector
**/
int maxInVec(vector<int> list)
{
	// Set the first element as the max value
	int maxVal = list.at(0); // list[0] 
	
	// Using iterator to go through each element in the vector starting at index 1
	for(vector<int>::iterator i = list.begin() + 1; i != list.end(); i++) // for (int i = 0; i < list.size(); i++)
	{
		// If the current element (*i) is greater than maxVal, assign *i to maxVal
		// NOTE: *i gives you the content of the vector at location i
		//			while i gives you the index location in the vector
		if (*i > maxVal)
			maxVal = *i;
	}
	
	// Have went through the entire vector, return the max value
	return maxVal;
}

/**
	Function:		int countAndErase(vector<int>& list, int target)
	Parameter:	
					list	A reference of the input vector, notice the '&'
					target	An integer value I'm looking for
	Return value:
					1) A modified list, with the target value(s) removed
					2) A count of the occurrences of target in the vector (or you can see it as the number of target value(s) removed)
**/
int countAndErase(vector<int>& list, int target)
{
	// Initialize count to 0
	int count = 0;
	
	// Go through each element in the vector
	for(vector<int>::iterator i = list.begin(); i != list.end(); i++)
	{
		// Check if the current element is the target I'm looking for.
		if (*i == target)
		{
			count++;		// Found a target. Increment count
			list.erase(i);	// Remove that target value. Note that vector will get automatically resized (shrink by 1), elements will get left-shifted (assuming the vector fills in from left to right)
			i--;			// Due to automatic resizing (above), i is decremented to avoid skipping over the next element in the vector to check
		}
	}
	
	// Return how many repeated target value(s)
	return count;
}