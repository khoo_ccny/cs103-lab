FILENAME:	lab-note-00.txt
AUTHOR:		Wai Khoo
DATE:		1/31/2014

NOTE:		Commands are inside single quotes; execute it on the command prompt without the quotes.

* Familiarize with VM and Linux
	* Try out various commands
		* cd
			* change directory
			* usage:
				* cd <directory>
			* example:
				* 'cd Desktop'  					-- traverse into the Desktop folder
				* 'cd home/cs103/csc103-tinkering'	-- traverse 3 levels deep into csc103-tinkering folder
				* 'cd ..'							-- go back (or up) one level
				* 'cd ../..'						-- go back (or up) TWO levels
		* pwd
			* path of current working directory
			* usage:
				* pwd
			* example:
				* Assuming I'm in csc103-tinkering folder as the example above, running 'pwd' will print 'home/cs103/csc103-tinkering'
		* ls
			* list files
			* usage:
				* ls [OPTION]
			* option:
				* -all	-- list all files with its properties such as permission, date modified, and size
				* and many more ...
			* example:
				* Assuming I'm in csc103-tinkering folder as the example above, running 'ls' will print
					00 01 test terminology.txt
			* note:
				* The result of the print is usually color-coded.
				* In general, white means individual file; green means executable; purple means folders; etc
		* mkdir
			* make directory
			* usage:
				* mkdir <new_directory>
			* example:
				* mkdir my_new_folder
		* rm
			* remove
			* usage:
				* rm [OPTION] FILE ...
			* option:	
				* -r	-- remove directories and their contents recursively
			* example
				* 'rm lab-note-00.txt'		-- remove the text file
				* 'rm -r csc103-tinkering'	-- remove the directory and its content
		
	* Code compilation
		* 1st way
			* Default
				* 'g++ main.cpp'					-- use the default executable filename: a.out
				* './a.out'
			* Customized
				* 'g++ main.cpp -o helloworld'		-- the -o flag is to specify a different output executable filename
				* './helloworld'
		* 2nd way
			* Default
				* 'g++ -c main.cpp'					-- COMPILE source code into OBJECT code
				* 'g++ main.o'						-- LINK OBJECT code into EXECUTABLE
				* './a.out'
			* Customized
				* 'g++ -c main.cpp'
				* 'g++ main.o -o helloworld2'
				* './helloworld2'
				
	* Editor
		* There are various tools that you can use, pick one that you like.
		* Partial list of editors:
			* edit (or medit)  	-- probably the easiest one to use, especially for those Windows user. It's similar to notepad
			* vi (or vim)		-- probably the hardest. all keyboard based, need to memorize (or have a cheatsheet handy) various editing command
			* emacs				
			* eclipse			-- more of an IDE (integrated development environment); can edit and run code from the same place
			* sublime text
				
	* Observation
		* Typical bugs you will run into are:
			* Typo
			* Missing semicolon
			* Missing/misused operator
			
	* Recommendation
		* Whatever editor you're using, be familiar with it, especially the syntax highlighting feature. That is, C++ keywords are one color, variables are another color, string in double quotes are another color, etc.