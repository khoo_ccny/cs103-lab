#include<iostream>
#include<cmath>		// Give me access to sqrt() and pow()
#include<cstring>
using namespace std;

int main() {
	// Declare my input and output variables
	double m_a, m_b, m_c, m_root1, m_root2;	
	
	// String to contain user response. Initially set to "y", otherwise the while block will never execute.
	string m_response = "y";				
	
	// Keep running if the response is "y" or "Y"
	while (m_response == "y" || m_response == "Y")
	{ 
		cout << "\n==== My amazing quadratic solver ====\n";
		cout << "Please input 3 values:";
		cin >> m_a >> m_b >> m_c; 
	
		// Compute the discriminant 
		double m_discriminant = m_b * m_b  - 4 * m_a * m_c; // either b*b or pow(b, 2)
	
		cout << "Solving ax^2 + bx + c = 0 ....\n\n";
		
		if (m_a == 0)
		{
			// Root can still be calcuate if a == 0 and b != 0
			if (m_b != 0)
			{
				m_root1 = -m_c / m_b;
				cout << "Root 1: " << m_root1 << endl;
			}
		}
		else if (m_discriminant < 0) // calculate imaginary roots
		{
			double value1, value2;
			value1 = (-m_b) / (2 * m_a);
			value2 = sqrt(-m_discriminant) / (2 * m_a); // sqrt(-1) can be factor out as i
			cout << "This equation does not has any real root.\n";
			cout << "Complex root 1: " << value1 << " + " << value2 << "i" << endl;
			cout << "Complex root 2: " << value1 << " - " << value2 << "i" << endl;
		}
		else if (m_discriminant == 0) // Calculate one root
		{
			m_root1 = -m_b / (2 * m_a);
			cout << "Root 1: " << m_root1 << endl;
		}
		else // There's real numbers to calculate
		{
			m_root1 = (-m_b + sqrt(m_discriminant)) / (2 * m_a);
			m_root2 = (-m_b - sqrt(m_discriminant)) / (2 * m_a);
		
			cout << "Root 1: " << m_root1 << endl;
			cout << "Root 2: " << m_root2 << endl;
		}
		
		cout << "\nDo you want to continue? [y/n]";
		cin >> m_response;
	}
	return 0;
}