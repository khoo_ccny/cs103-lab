#include <iostream>
using namespace std;

int main() {
	// Declare input and output variables
	int m_numSec, m_hours, m_minutes, m_seconds;
	
	cout << "\n==== My amazing time conversion program ====\n";
	cout << "Enter number of seconds: ";
	cin >> m_numSec; // Could be any values
	
	// Error checking
	if (m_numSec < 0)
	{
		// Prompting user of an error
		// If the user type in a negative, I simply just want to quit the program
		cout << "Your value must be positive! I cannot work with negative values! Quitting now! Bye!\n";
		
		// This will return the main function (essentially your program) to the operating system, which is the same as exiting/quitting the program.
		return 0; 
	}
	
	// There's valid input to process...
	
	m_hours		= m_numSec / 3600;		// Compute # of hour(s) in m_numSec
	int dummy	= m_numSec % 3600;		// Store the remainder in a dummy variable
	m_minutes	= dummy / 60;			// Compute # of minute(s) in dummy
	m_seconds	= dummy % 60;			// The remainder of dummy is # of second(s)

	// Purely for formatting output such that it shows proper grammar.
	// Also introduce a userful conditional ternary operator (?). Check below link for more details
	// http://www.cplusplus.com/doc/tutorial/operators/
	// If any value is greater than 1, return the plural form which is the first part before :
	// Otherwise, return the singular form which is the second part after :
	string str_hr	= (m_hours > 1)		? "Hours"	: "Hour";
	string str_min	= (m_minutes > 1)	? "Minutes" : "Minute";
	string str_sec	= (m_seconds > 1)	? "Seconds" : "Second";
	
	cout << str_hr << " " << m_hours << endl << str_min << " " << m_minutes << endl << str_sec << " " << m_seconds << endl;

	return 0; 
}