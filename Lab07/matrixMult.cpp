/**
	Problem Statement:
				Write a program using vector to do 2D matrix multiplication. Assume square matrices. Inputs can be obtained from user or randomly generated
				
	Compile:	g++ -Wall matrixMult.cpp -o matrixMult
	Execute: 
				./matrixMult [size]
	Note:		-Wall tells the compiler to display all warnings, if any. -W is the warning option. all specify all
				Reference to matrix multiplication: http://en.wikipedia.org/wiki/Matrix_multiplication
**/
#include<iostream>
#include<ctime>		// provide time()
#include<cstdlib>	// provide srand() and atoi()
#include<vector>
using namespace std;

// FUnction prototype
void printMatrix(const vector<vector<int> >&);	// print a matrix

int main(int argc, char *argv[])		// command line arguments; the program name will always get passed into main() as argv[0]
{
	// Default value that can be changed via command line
	int m_size = 3;
	
	switch(argc)
	{
		case 2:
			m_size = atoi(argv[1]);		// Convert the 2nd argument from char to int; specify size
		case 1:							// No additional argument supplied; do nothing
			break;
		default:						// Wrong usage. Exit
			cout << "Invalid number of argument!\n";
			cout << "./matrixMult [size]\n";
			return 0;
	}
	
	/*
		Initialize three 2D vectors of size (m_size X m_size)
		In general, a matrix of type int, should be declared as vector<vector<int> > a(numRow, vector<int>(numCol))
		where numRow specify the number of rows in the matrix and numCol specify the number of column in the matrix.
		
		Conceptually, each row points to a vector<int> of size numCol. The variable a can also be read as "vector of vectors of type int"
	*/
	vector<vector<int> > m_a(m_size, vector<int>(m_size)), m_b(m_size, vector<int>(m_size)), m_c(m_size, vector<int>(m_size));
	
	/*
		// Alternate way of initialize 2D vectors
		vector<vector<int> > m_a;
		m_a.resize(numRow);
		for(size_t i = 0; i < numRow; i++)
			m_a[i].resize(numCol);
	*/
	
	srand(time(NULL));	// Seed the random number generator
	
	/*
		Going through each element in the matrix and randomly assign a value to it, except the resultant matrix.
		
		To iterate through a 1D array, you need a for (or while) loop. For 2D array, you need 2 nested for loops. In general, to iterate through a N-D array (where N is an integer value), you will need N nested for loops. 
	*/
	for(size_t i = 0; i < m_a.size(); i++)			// Loop through each row; m_a.size() returns the number of rows
	{
		for(size_t j = 0; j < m_a[i].size(); j++)	// Loop through each column; m_a[i].size() returns the number of columns (of row_i)
		{
			m_a[i][j] = rand() % 10;				// Randomly generate value between 0 to 9, inclusive and assign to a
			m_b[i][j] = rand() % 10;				// Similarly for b
			m_c[i][j] = 0;							// Initialize the resultant matrix to be all 0
		}
	}
	
	// Print the content of matrices a and b 
	cout << "Matrix a is \n";
	printMatrix(m_a);
	cout << "\nMatrix b is \n";
	printMatrix(m_b);
	
	// Matrix multiplication
	// The first two outer loops can be seen as iterating through the resultant matrix
	for(size_t i = 0; i < m_a.size(); i++)			// Loop through each  row
	{
		for(size_t j = 0; j < m_a[i].size(); j++)	// Loop through each column
		{
			for(size_t k = 0; k < m_a[i].size(); k++) // Loop through each element in the row/column
				m_c[i][j] += m_a[i][k] * m_b[k][j]; // Multiply the corresponding k-th element and sum it up
		}
	}
	
	// Print the content of matrix c
	cout << "\nResult is\n";
	printMatrix(m_c);
	
	return 0;
}

/**
	Function:		void printMatrix(const vector<vector<int> >& mat)
	Parameter:	
					mat		The 2D vector.
	Note:
					Although one can do pass-by-value. It's actually more memory efficient if we pass-by-reference. Having a const in front of the formal paramter simply safeguard that the content of mat won't be changed by this function (accident or otherwise).
**/
void printMatrix(const vector<vector<int> >& mat)
{
	for(size_t i = 0; i < mat.size(); i++)			// Loop through each row
	{
		for(size_t j = 0; j < mat[i].size(); j++)	// Loop through each column
			cout << mat[i][j] << "\t";				// Print the entry at (i,j)
		cout << endl;			// Notice that this is outside the 2nd for loop but inside the 1st for loop.
	}
}